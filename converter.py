import glob, os, sys, shutil
import re


def checkIfFolderAlreadyExists():
    try:
        os.mkdir("output")
    except Exception as e:
        print("VERZEICHNIS EXISTIERT")

def clean():
    if(sys.argv[1] == "clean"):
        try:
            sure = input("Verzeichnis Output wird gelöscht sind Sie sich Sicher? y/n: ")
            if(sure == "y"):
                shutil.rmtree("output", ignore_errors=True)
            exit()
        except Exception as e:
            print(e)

def convertXMLtoVTT():
    # loop through all files in the directory
    for idx, file in enumerate(glob.glob("*.xml")):
        f = open(file, "r")
        subs = f.read()
        f.close()

        # remove all whitespaces and newlines
        subs = subs.replace('  ', '', -1)
        subs = subs.replace('\n', '', -1)
        subs = subs.replace('\t', '', -1)

        # regex to remove <?xml
        subs = re.sub(r'<\?xml.*?>', '', subs)

        # replace unwanted characters
        subs = subs.replace("<?xml version='1.0' encoding='utf-8'?>", '').replace('<subtitles>', '').replace('</subtitles>', '').replace('\n', '').replace('</end><text>', '\n').replace('<sub>', 'WEBVTT\n\n1\n', 1).replace('</start><end>', '-->').replace('<start>', '').replace('</text>', '\n')
        count = subs.count('</sub><sub>') + 2

        # find all time periods matching the pattern example --> 5.62 --> 7.62
        regex = r'\d{1,3}\.\d{1,3}-->\d{1,3}\.\d{1,3}'
        allTimeGroups = re.findall(regex, subs) # example ['5.67-->7.67', '7.67-->9.67'] etc.

        #  loop through all time groups
        for periodOfTimes in allTimeGroups:
            # split the periods to single times (example: [5.67, 7.67])
            regex = r'\d{1,3}\.\d{1,3}'
            periodOfTime = re.findall(regex, periodOfTimes) # example ['5.67', '7.67'] etc.
            finishedNewTimeLine = ""
            # loop through all times
            for idx, time in enumerate(periodOfTime):
                # format times correctly --> example: from 67.776 to 00:01:07:775
                hours = int(float(time) // 3600)
                minutes = int((float(time) % 3600) // 60)
                seconds = int((float(time) % 3600) % 60)
                milliseconds = int((float(time) % 1) * 1000)
                output = "{:02d}:{:02d}:{:02d}:{:03d}".format(hours ,minutes, seconds, milliseconds)
                if(idx == 0):
                    # on first iteration add output-->
                    finishedNewTimeLine += output + "-->"
                elif(idx == 1):
                    # on second iteration just concatenate output
                    finishedNewTimeLine += output

            # replace old time line with new one
            subs = subs.replace(periodOfTimes, finishedNewTimeLine)

        for i in range(2, count):
            subs = subs.replace('</sub><sub>', "\n" + str(i) + "\n", 1)

        subs = subs.replace("</sub>", "")
        writeFile(subs, file)

def writeFile(subs, file):
    w = open("output/" +file.replace('.xml', '') + ".vtt", 'w')
    w.write(subs)
    w.close()

# def if main
if __name__ == "__main__":
    if(len(sys.argv) > 1):
        clean()
    checkIfFolderAlreadyExists()
    try:
        convertXMLtoVTT()
        print("Konvertierung erfolgreich. Die konvertierten Dateien befinden sich im Output Ordner")
    except Exception as e:
        print(e)
